const sum = require('./sum');

test('adds 1 + 2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3);
});

test('adds 0 + 7 to equal 7', () => {
    expect(sum(0, 7)).toBe(7);
});

test('adds "1" + "1" to equal 2', () => {
    expect(sum("1", "1")).toBe(2);
});