const fakultaet = require('./fakultaet');

test('0! -> 1', () => {
    expect(fakultaet(0)).toBe(1);
});