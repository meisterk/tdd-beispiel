import { fibonacci } from "./fibonacci.js";


// 1 -> 1
if (fibonacci(1) === 1) {
    console.log('green')
} else {
    console.log('red');
}


// 2 -> 1
if (fibonacci(2) === 1) {
    console.log('green')
} else {
    console.log('red');
}

// 3 -> 2
if (fibonacci(3) === 2) {
    console.log('green')
} else {
    console.log('red');
}

// 4 -> 3
if (fibonacci(4) === 3) {
    console.log('green')
} else {
    console.log('red');
}

// 5 -> 5
if (fibonacci(5) === 5) {
    console.log('green')
} else {
    console.log('red');
}

// 6 -> 8
if (fibonacci(6) === 8) {
    console.log('green')
} else {
    console.log('red');
}

// 30 -> 832 040
if (fibonacci(30) === 832040) {
    console.log('green')
} else {
    console.log('red');
}

// 50 -> 12586269025
if (fibonacci(50) === 12586269025) {
    console.log('green')
} else {
    console.log('red');
}